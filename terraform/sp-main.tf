### DATA SOURCES ###
data "azurerm_subscription" "main" {}

variable "service-principals" {
  description = "Create service principals with these names as contributors"
  type = list(string)
  default = ["aks-apps"] #, "tf-contributor"]
}


### IDENTITIES ###

## Terraform SP for creating infrastructure resources
# Create Azure AD App
resource "azuread_application" "SP-APP" {
  count = length(var.service-principals)
  name = "${terraform.workspace}_${var.service-principals[count.index]}"
  available_to_other_tenants = false
}

# Create Service Principal associated with the Azure AD App
resource "azuread_service_principal" "SP" {
  count = length(var.service-principals)
  application_id = azuread_application.SP-APP[count.index].application_id
}

# Generate random password to be used for Service Principal password
resource "random_password" "PW" {
  count = length(var.service-principals)
  length = 32
  special = true
}

# Create Service Principal password
resource "azuread_service_principal_password" "SP-PW" {
  count = length(var.service-principals)
  service_principal_id = azuread_service_principal.SP[count.index].id
  value = random_password.PW[count.index].result
  end_date_relative = "17520h" #expire in 2 years
}

# Create role assignment for service principal
resource "azurerm_role_assignment" "ROLE-CONTRIBUTOR" {
  count = length(var.service-principals)
  scope = data.azurerm_subscription.main.id
  role_definition_name = "Contributor"
  principal_id = azuread_service_principal.SP[count.index].id
}


### OUTPUTS ###
output "display_name" {
#  count = length(var.service-principals)
  value = azuread_service_principal.SP.*.display_name
}

output "client_id" {
#  count = length(var.service-principals)
  value = azuread_application.SP-APP.*.application_id
}

output "client_secret" {
#  count = length(var.service-principals)
  value = azuread_service_principal_password.SP-PW.*.value
  sensitive = false
}

output "object_id" {
#  count = length(var.service-principals)
  value = azuread_service_principal.SP.*.id
}
