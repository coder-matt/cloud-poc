### INFRASTRUCTURE ###

#TODO: remove hard code reference to list index 

resource "azurerm_kubernetes_cluster" "AKS-CLUSTER-APPS" {

  dns_prefix = "aks-cluster"
  location = azurerm_resource_group.AKS-APPS.location
  name = "aks-cluster-apps"
  resource_group_name = azurerm_resource_group.AKS-APPS.name
  default_node_pool {
    name       = "default"
    node_count = 2
    vm_size    =  "Standard_B2s" #"Standard_F2s_v2"
  }
  service_principal {
    client_id     = azuread_application.SP-APP.0.application_id
    client_secret = azuread_service_principal_password.SP-PW.0.value
  }
}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.AKS-CLUSTER-APPS.kube_config.0.client_certificate
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.AKS-CLUSTER-APPS.kube_config_raw
}
