### CONTANIERS ###
resource "azurerm_container_group" "ACI-VOTING-APP" {
  name                = "aci-voting-app"
  location            = azurerm_resource_group.AKS-APPS.location
  resource_group_name = azurerm_resource_group.AKS-APPS.name
  ip_address_type     = "public"
  dns_name_label      = "aci-voting-app"
  os_type             = "linux"

  container {
    name   = "aci-voting-app"
    image  = "microsoft/azure-vote-front:cosmosdb"
    cpu    = "0.5"
    memory = "1.5"
    ports {
      port     = 80
      protocol = "TCP"
    }

    environment_variables = {
      "TITLE"               = "Azure Voting App"
      "VOTE1VALUE"          = "Apples"
      "VOTE2VALUE"          = "Bananas"
    }

    secure_environment_variables = {
      "COSMOS_DB_ENDPOINT"  = azurerm_cosmosdb_account.COSMOSDB-APPS.endpoint
      "COSMOS_DB_MASTERKEY" = azurerm_cosmosdb_account.COSMOSDB-APPS.primary_master_key
    }
  }
}


### OUTPUTS ###
output "dns" {
  value = azurerm_container_group.ACI-VOTING-APP.fqdn
}

output "endpoint" {
    value = azurerm_cosmosdb_account.COSMOSDB-APPS.endpoint
}
