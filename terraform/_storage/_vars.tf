### VARIABLES ###
variable "location" {
  type = string
  default = "westeurope"
}

variable "resource_group" {
  type = string
  default = "TF-STORE"
}
