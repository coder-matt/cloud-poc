resource "azurerm_resource_group" "RG" {
  name     = upper("${terraform.workspace}-${var.resource_group}")
  location = var.location
}

resource "random_integer" "POSTFIX" {
  min = 10000
  max = 99999
}

resource "azurerm_storage_account" "SA" {
  name                     = lower("tfrs${random_integer.POSTFIX.result}")
  resource_group_name      = azurerm_resource_group.RG.name
  location                 = azurerm_resource_group.RG.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "azurerm_storage_container" "CON" {
  name                  = "tf-state"
  storage_account_name  = azurerm_storage_account.SA.name
  container_access_type = "private"
}

data "azurerm_storage_account_sas" "SAS" {
  connection_string = azurerm_storage_account.SA.primary_connection_string
  https_only = true

  resource_types {
      service = true
      container = true
      object = true
  }

  services {
      blob = true
      table = false
      queue = false
      file = false
  }

  start = timestamp()
  expiry = timeadd(timestamp(), "17520h")

  permissions {
      read = true
      write = true
      delete = true
      list = true 
      add = true
      create = true
      update = false
      process = false
  }
}

resource "null_resource" "POST-CONFIG" {
    depends_on = [azurerm_storage_container.CON]

    provisioner "local-exec" {
        command = <<EOT
echo 'storage_account_name = "${azurerm_storage_account.SA.name}"' > _backend-config.txt
echo 'container_name = "tf-state"' >> _backend-config.txt
echo 'key = "terraform.tfstate"' >> _backend-config.txt
echo 'sas_token = "${data.azurerm_storage_account_sas.SAS.sas}"' >> _backend-config.txt
EOT
    }
}


output "resource_group" {
  value = azurerm_resource_group.RG.name
}

output "storage_acccount_name" {
  value = azurerm_storage_account.SA.name
}

output "sas_token" {
  value = data.azurerm_storage_account_sas.SAS.sas
}
