### INFRASTRUCTURE ###
resource "azurerm_resource_group" "AKS-APPS" {
  name     = upper("${terraform.workspace}-${var.resource_group}")
  location = var.location

  # tags = {
  #   Environment = upper(${terraform.workspace})
  #   Team = "Infrastructure Platform"
  # }
}