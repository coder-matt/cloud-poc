## INFRASTRUCTURE ##
resource "azurerm_virtual_network" "AKS-APPS" {
  name = "vnet_${lower(azurerm_resource_group.AKS-APPS.name)}"

  address_space = [
    "10.0.0.0/16"
  ]
  location = azurerm_resource_group.AKS-APPS.location
  resource_group_name = azurerm_resource_group.AKS-APPS.name
}

