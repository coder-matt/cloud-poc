## INFRASTRUCTURE ##
resource "azurerm_container_registry" "ACR" {
  name                     = "DLAPApps1"
  resource_group_name      = azurerm_resource_group.AKS-APPS.name
  location                 = azurerm_resource_group.AKS-APPS.location
  sku                      = "Standard"
  admin_enabled            = false
#  georeplication_locations = ["North Europe","UK South"]
}