### VARIABLES ###
variable "location" {
  type = string
  default = "westeurope"
}

variable "resource_group" {
  type = string
  default = "AKS-APPS"
}

variable "default_tags" { 
    type = map 
    default = { 
      department: "it",
      managedby: "terraform",
      application: "my-first-app"
  } 
}
