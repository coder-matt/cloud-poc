### DATASTORE ###
resource "azurerm_cosmosdb_account" "COSMOSDB-APPS" {
  
  name = "ds-cosmosdb-apps-1"
  resource_group_name = azurerm_resource_group.AKS-APPS.name
  location = azurerm_resource_group.AKS-APPS.location
  offer_type = "Standard"
  kind = "MongoDB"
  consistency_policy {
    consistency_level = "Session"
  }

  geo_location {
    failover_priority = 0
    location = azurerm_resource_group.AKS-APPS.location
  }

  # tags {
  #   environment = upper("${terraform.workspace}")
  #   manager = "terraform"
  #  }
}


