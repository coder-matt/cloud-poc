### DATA SOURCES ###
data "azurerm_subscription" "MAIN" {}

variable "contributors" {
  description = "Create service principals with these names as contributors"
  type = list(string)
  default = ["aks-apps", "tf-contributor"]
}


### IDENTITIES ###

## Terraform SP for creating infrastructure resources
# Create Azure AD App
resource "azuread_application" "SP-APP" {
  count = length(var.contributors)
  name = "${terraform.workspace}_var.contributors[count.index]"
  available_to_other_tenants = false
}

# Create Service Principal associated with the Azure AD App
resource "azuread_service_principal" "SP" {
  count = length(var.contributors)
  application_id = azuread_application.SP-APP[count.index].application_id
}

# Generate random password to be used for Service Principal password
resource "random_password" "PW" {
  count = length(var.contributors)
  length = 32
  special = true
}

# Create Service Principal password
resource "azuread_service_principal_password" "SP-PW" {
  count = length(var.contributors)
  service_principal_id = azuread_service_principal.SP[count.index].id
  value = random_password.PW[count.index].result
  end_date_relative = "17520h" #expire in 2 years
}

# Create role assignment for service principal
resource "azurerm_role_assignment" "ROLE-CONTRIBUTOR" {
  count = length(var.contributors)
  scope = data.azurerm_subscription.MAIN.id
  role_definition_name = "Contributor"
  principal_id = azuread_service_principal.SP[count.index].id
}


### OUTPUTS ###
output "display_name" {
  value = azuread_service_principal.SP.*.display_name
}

output "client_id" {
  # count = length(var.contributors)
  value = azuread_application.SP-APP.*.application_id
}

output "client_secret" {
  # count = length(var.contributors)
  value = azuread_service_principal_password.SP-PW.*.value
  sensitive = false
}

output "object_id" {
  # count = length(var.contributors)
  value = azuread_service_principal.SP.*.id
}
