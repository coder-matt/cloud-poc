﻿## GENERAL
$resource_group = 'APPONE'
$location = 'westeurope'

# Create Master resource group
New-AzResourceGroup -Name $resource_group.ToUpper() -Location $location -Force -Confirm



## NETWORK ##
# Create Virtual Network
$vnet = New-AzVirtualNetwork -Name "vnet_$($resource_group)".ToLower() -ResourceGroupName $resource_group -Location $location -AddressPrefix '10.0.0.0/16' -Force -Confirm

# Create Network Security Group for following subnet, albeit not used by Cosmos once Service Endpoint enabled, so might remove this ?
$nsg = New-AzNetworkSecurityGroup -Name 'nsg_data' -ResourceGroupName $resource_group -Location $location -Force -Confirm

# Create Subnet for 'data' resources
Add-AzVirtualNetworkSubnetConfig -Name 'snet_data' -AddressPrefix '10.0.0.0/24' -VirtualNetwork $vnet -NetworkSecurityGroup $nsg
$vnet | Set-AzVirtualNetwork
#Note: this creates a NetworkWatcherRG resource group, show hidden types displays a microsoft.network/networkwatchers object



## COSMOS DB ##
# Note: cannot create a 3.6 version api via scripting, only portal currently

$resource_account = 'Microsoft.DocumentDb/databaseAccounts'
$api_version = '2015-04-08'
$account_name = 'dba-cosmos-apps-1'

$CosmosDBProperties = @{
    "databaseAccountOfferType" = "Standard";
    "locations" = @(@{ "locationName"="West Europe"; "failoverPriority"=0 }, @{ "locationName"="UK South"; "failoverPriority"=1 });
    "consistencyPolicy" = @{ "defaultConsistencyLevel" = "Session" };
    "enableMultipleWriteLocations" = "false"
}

# Cosmos Account / Api    
New-AzResource -ResourceType $resource_account -ApiVersion $api_version -Kind 'MongoDB' -ResourceGroupName $resource_group -Location $location -Name $account_name -PropertyObject $CosmosDBProperties

# Cosmos Database and single collection
$database_name = 'influx'
$db_resource = "$($resource_account)/apis/databases"
$db_path = "$($account_name)/mongodb/$($database_name)"
$collection_name = 'changes'

New-AzResource -ResourceType $db_resource -ApiVersion $api_version -ResourceGroupName $resource_group -Name $db_path -PropertyObject @{ "resource"=@{ "id"=$database_name }} -Force -Confirm
New-AzResource -ResourceType "$db_resource/collections" -ApiVersion $api_version -ResourceGroupName $resource_group -Name "$db_path/$($collection_name)" -PropertyObject @{"resource"=@{"id"="$($collection_name)"}} -Force -Confirm



## KUBERNETES CLUSTER ##
#az aks create --name 'aks-cluster-apps-1' --resource-group 'APPONE' --node-count 1 --generate-ssh-keys

$aks_name = 'aks-cluster-apps-1'
#New-AzAks -ResourceGroupName $resource_group -Name $aks_name -NodeCount 1 -Location $location -DnsNamePrefix $aks_name -Confirm #-NodeVmSize 'Standard_B2s' 
New-AzAks -ResourceGroupName $resource_group -Name $aks_name -NodeCount 1 -Location 'westeurope' -DnsNamePrefix $aks_name -KubernetesVersion '1.15.10' -NodeVmSize 'Standard_F2s_v2' -Confirm

