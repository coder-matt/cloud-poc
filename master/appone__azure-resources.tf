### PROVIDERS ###
provider "azurerm" {
  version = "~>2.0"
  features{}
}


### VARIABLES ###
variable "location" {
  type = string
  default = "westeurope"
}

variable "resource_group" {
  type = string
  default = "POC_AKS-APPS"
}


### INFRASTRUCTURE ###
resource "azurerm_resource_group" "AKS-APPS" {
  name     = var.resource_group
  location = var.location
}

resource "azurerm_virtual_network" "VNET_POC_AKS-APPS" {
  name = "vnet_${lower(azurerm_resource_group.AKS-APPS.name)}"

  address_space = [
    "10.0.0.0/16"
  ]
  location = azurerm_resource_group.AKS-APPS.location
  resource_group_name = azurerm_resource_group.AKS-APPS.name
}

# resource "azurerm_network_security_group" "nsg_data" {
#   location = var.location
#   name = "nsg_data"
#   resource_group_name = azurerm_resource_group.APPONE.name
# }

# resource "azurerm_subnet" "snet_data" {
#   address_prefix = "10.0.0.0/24"
#   name = "snet_data"
#   resource_group_name = azurerm_resource_group.APPONE.name
#   virtual_network_name = azurerm_virtual_network.VNET_APPONE.name
# }

# resource "random_integer" "ri" {
#   min = 10000
#   max = 99999
# }

resource "azurerm_cosmosdb_account" "COSMOSDB-APPS" {
  
  name = "acc-cosmosdb-apps"
  resource_group_name = azurerm_resource_group.AKS-APPS.name
  location = azurerm_resource_group.AKS-APPS.location
  offer_type = "Standard"
  kind = "MongoDB"
  consistency_policy {
    consistency_level = "Session"
  }
  geo_location {
    failover_priority = 0
    location = azurerm_resource_group.AKS-APPS.location
  }
}

resource "azurerm_cosmosdb_mongo_database" "INFLUX-DB" {
  
  name                = "influx"
  resource_group_name = azurerm_resource_group.AKS-APPS.name
  account_name        = azurerm_cosmosdb_account.COSMOSDB-APPS.name
}

resource "azurerm_cosmosdb_mongo_collection" "CHANGES-COL" {

  name                = "changes"
  resource_group_name = azurerm_resource_group.AKS-APPS.name
  account_name        = azurerm_cosmosdb_account.COSMOSDB-APPS.name
  database_name       = azurerm_cosmosdb_mongo_database.INFLUX-DB.name

  default_ttl_seconds = "777"
  shard_key           = "uniqueKey"
  throughput          = 400
}


### KUBERNETES CLUSTER ###
# module "aks" {
#   source              = "Azure/aks/azurerm"
#   version             = "3.0.0"
#   resource_group_name = azurerm_resource_group.AKS-APPS.name
#   client_id           = "014f4935-3ece-4a2a-be73-9847239a8759"
#   client_secret       = "?wSTP2=xauU[O3[Yke5b0C]y]XX3mNIo"
#   prefix              = "aks"
# }




resource "azurerm_kubernetes_cluster" "AKS-APPS-CLUSTER" {

  dns_prefix = "aks-cluster"
  location = azurerm_resource_group.AKS-APPS.location
  name = "aks-cluster-apps"
  resource_group_name = azurerm_resource_group.AKS-APPS.name
  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_F2s_v2" #"Standard_B2s"
  }
  service_principal {
    client_id     = "9fe24b92-4e03-4b51-98c6-3194f2dbedae"
    client_secret = "Aej({]*WY+VZdD$7bSx(xkO?Pnq0OTWo"
  }
}

output "client_certificate" {
  value = azurerm_kubernetes_cluster.AKS-APPS-CLUSTER.kube_config.0.client_certificate
}

output "kube_config" {
  value = azurerm_kubernetes_cluster.AKS-APPS-CLUSTER.kube_config_raw
}
