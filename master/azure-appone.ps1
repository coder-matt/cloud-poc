
$resource_group = 'APPONE'
$location = 'westeurope'
$api = '2015-04-08'
$account_name = 'dba-cosmos-apps-one'
$resource_account = 'Microsoft.DocumentDb/databaseAccounts'


Function New-xResourceGroup(){

    # Param(
    #     [Parameter(Mandatory)][string]$GroupName
    # )

    New-AzResourceGroup -Name $resource_group.ToUpper() -Location $location -Confirm
}


Function Remove-xResourceGroup(){

    Param(
        [Parameter(Mandatory)][string]$GroupName
    )

    Remove-AzResourceGroup -Name $GroupName -Force -Confirm
}


Function New-xVirtualNetwork(){
    New-AzVirtualNetwork -Name "vnet_$($resource_group)".ToLower() -ResourceGroupName $resource_group -Location $location -AddressPrefix '10.0.0.0/16' -Confirm
}

Function New-xVNetSubnet() {

    Param(
        [Parameter(Mandatory)][string]$SubnetName,
        [Parameter(Mandatory)][string]$AddressPrefix #= '10.0.0.0/24'
    )

    $nsg = New-AzNetworkSecurityGroup -Name "nsg_$($SubnetName)".ToLower() -ResourceGroupName $resource_group -Location $location -Force -Confirm

    # Create Subnets (create subnet config then set it to vnet)
    $vnet = Get-AzVirtualNetwork -Name "vnet_$($resource_group)" -ResourceGroupName $resource_group
    Add-AzVirtualNetworkSubnetConfig -Name "snet_$($SubnetName)".ToLower() -AddressPrefix $AddressPrefix -VirtualNetwork $vnet -NetworkSecurityGroup $nsg
    $vnet | Set-AzVirtualNetwork

}



Function New-xCosmosAccount(){

    # Note: cannot create a 3.6 version api via scripting, only portal currently


    $CosmosDBProperties = @{
        "databaseAccountOfferType" = "Standard";
        "locations" = @(@{ "locationName"=$location; "failoverPriority"=0 }, @{ "locationName"="UK South"; "failoverPriority"=1 });
        "consistencyPolicy" = @{ "defaultConsistencyLevel" = "Session" };
        "enableMultipleWriteLocations" = "false"
    }
    
    New-AzResource -ResourceType $resource_account -ApiVersion $api -Kind 'MongoDB' -ResourceGroupName $resource_group -Location $location -Name $account_name -PropertyObject $CosmosDBProperties
}


Function New-xCosmosDatabase(){

    Param(
        [Parameter(Mandatory)][string]$DatabaseName,
        [string]$CollectionName = $null
    )

    $db_resource = "$resource_account/apis/databases"
    $db_path = "$($account_name)/mongodb/$($DatabaseName)"

    New-AzResource -ResourceType $db_resource -ApiVersion $api -ResourceGroupName $resource_group -Name $db_path -PropertyObject @{ "resource"=@{ "id"=$DatabaseName }} -Force
    if ($CollectionName -ne $null) {
        New-AzResource -ResourceType "$db_resource/collections" -ApiVersion $api -ResourceGroupName $resource_group -Name "$db_path/changes" -PropertyObject @{"resource"=@{"id"="changes"}} -Force
    }
} 


Function Remove-xCosmosDatabase(){

    Param(
        [Parameter(Mandatory)][string]$DatabaseName
    )

    Get-AzResource -ResourceGroupName $resource_group -Name $account_name | Remove-AzResource
}


Function New-xAzureKubernetesService() {

    # az aks create --resource-group APPONE --name aks-cluster-apps-1 --node-count 1 --enable-addons monitoring --generate-ssh-keys


}





# Azure Container Service
# New-AzContainerGroup -ResourceGroupName 'CLOUDIE' -Name 'influx-ui' -Image nginx:latest -OsType Linux -DnsNameLabel 'influx' -Location 'UK South' -Cpu 1 -MemoryInGB 1.5 -Confirm

# Remove-AzContainerGroup -ResourceGroupName 'CLOUDIE' -Name 'influx-ui' -Confirm


# Kubernetes Cluster (AKS)
# New-AzAks -ResourceGroupName 'APPONE' -Name 'aks-cluster-apps-1' -NodeCount 1 -Location 'westeurope' -DnsNamePrefix 'aks-cluster-apps-1' -NodeVmSize 'S tandard_B2s' -Confirm






# CreateResourceGroup: APPONE
# CreateVirtualNetwork: vnet_appone
# Add snet_data subnet to virtual network
# Create Cosmos DB account, then database, then collection
# TODO - Manual: add vnet/snet_data then later private service endpoint see https://docs.microsoft.com/en-us/azure/cosmos-db/how-to-configure-private-endpoints#create-a-private-endpoint-by-using-azure-powershell
# Currently erroring when creating service endpoint, bad request
# Create AKS platform - https://docs.microsoft.com/en-us/azure/aks/kubernetes-walkthrough
# 

